import sys

class Person(object):
    def speak(self, message):
        """Speak out given message in stdout
                
                Parameters:
                    
                - `message`: A string going to speak
                
                Return:
                    
                - `self`: This instance
               """
        sys.stdout.write('%s\n' % message)
        return self

    def yell(self, message):
        """Yell out given message in both stdout and stderr

                Parameters:

                - `message`: A string going to yell

                Return:

                - `self`: This instance
               """
        sys.stdout.write('%s\n' % message)
        sys.stderr.write('%s!\n' % message)
        return self